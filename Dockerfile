FROM docker.io/ubuntu:latest

RUN apt-get update && apt-get install -y -qq \
      curl build-essential flex g++ git-svn libcairo2-dev libglib2.0-dev \
      libcups2-dev libgtkglext1-dev git-core libglu1-mesa-dev libnspr4-dev \
      libnss3-dev libgnome-keyring-dev libasound2-dev gperf bison libpci-dev \
      libkrb5-dev libgtk-3-dev libxss-dev python libpulse-dev ca-certificates \
      default-jre
RUN apt-get install -y -qq wget
RUN mkdir -p /cef/build  && cd /cef/build && wget https://bitbucket.org/chromiumembedded/cef/raw/master/tools/automate/automate-git.py
ENV GN_DEFINES 'is_official_build=true use_allocator=none symbol_level=1 ffmpeg_branding=Chrome proprietary_codecs=true'
ENV CFLAGS "-Wno-error"
ENV CXXFLAGS "-Wno-error"
ENV CEF_ARCHIVE_FORMAT tar.bz2
RUN mkdir -p /cef/source && cd /cef/build && python automate-git.py --download-dir=download \
      --url=https://github.com/tiliado/cef.git \
      --branch=3809 --checkout=3809-valacef \
      --force-clean --force-clean-deps --force-config \
      --x64-build --build-target=cefsimple --no-build --no-distrib
RUN cd /cef/build && python automate-git.py --download-dir=download \
      --url=https://github.com/tiliado/cef.git \
      --branch=3809 --checkout=origin/3809-valacef \
      --x64-build --build-target=cefsimple --no-update --force-build \
      --no-debug-build
